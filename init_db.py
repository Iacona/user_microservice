import sqlite3
from sqlite3 import Error

def create_connection(db_file):
    """ create a database connection to a SQLite database """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)

    return conn

def create_table(conn, create_table_sql):
    """ create a table from the create_table_sql statement
    :param conn: Connection object
    :param create_table_sql: a CREATE TABLE statement
    :return:
    """
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Error as e:
        print(e)

if __name__ == '__main__':
    sql_create_user_table = """ CREATE TABLE IF NOT EXISTS user  (
                                        id INTEGER PRIMARY KEY AUTOINCREMENT,
                                        name TEXT NOT NULL,
                                        surname TEXT NOT NULL,
                                        birthday DATE UNIQUE NOT NULL
                                    ); """
    conn = create_connection('./user.db')
    if conn is not None:
        # create projects table
        create_table(conn, sql_create_user_table)
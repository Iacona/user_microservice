from flask import Flask
from flask import request
from flask import jsonify
from flask_sqlalchemy import SQLAlchemy
import sqlite3 as sql

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///user.db'
db = SQLAlchemy(app)

db.Model.metadata.reflect(db.engine)

@app.route('/adduser',methods = ['PUT', 'POST'])
def adduser():
    if request.method == 'PUT':
        try:
            userId = request.args.get('id')
            name = request.args.get('name')
            surname = request.args.get('surname')
            birthday = request.args.get('birthday')
         
            with sql.connect("user.db") as con:
                cur = con.cursor()
                query = "UPDATE user SET name = '" + name + "', surname = '" + surname + "', birthday = '" + birthday + "' WHERE id == " + userId
                cur.execute(query)
            
                con.commit()
                msg = "Record successfully added"
        except:
            con.rollback()
            msg = "error in insert operation"
      
        finally:
            con.close()
            return msg
    elif request.method == 'POST':
        try:
            name = request.args.get('name')
            surname = request.args.get('surname')
            birthday = request.args.get('birthday')
         
            with sql.connect("user.db") as con:
                cur = con.cursor()
                query = "INSERT INTO user (name, surname, birthday) VALUES ('" + name + "','" + surname + "','" + birthday + "')" 
                cur.execute(query)
            
                con.commit()
                msg = "Record successfully added"
        except:
            con.rollback()
            msg = "error in insert operation"
      
        finally:
            con.close()
            return msg


if __name__ == '__main__':
    app.run(debug=True)